################################################################################
#
# VBAM
#
################################################################################

LIBRETRO_VBAM_VERSION = 331ba35aeb09ba24bb9d7368645d26e6254461fa
LIBRETRO_VBAM_SITE = $(call github,libretro,vbam-libretro,$(LIBRETRO_VBAM_VERSION))

ifeq ($(BR2_ARM_CPU_HAS_NEON),y)
LIBRETRO_VBAM_NEON += "HAVE_NEON=1"
else
LIBRETRO_VBAM_NEON += "HAVE_NEON=0"
endif

ifeq ($(BR2_arm),y)	
LIBRETRO_VBAM_PLATFORM=classic_armv7_a7
else
LIBRETRO_VBAM_PLATFORM=$(RETROARCH_LIBRETRO_PLATFORM)
endif

define LIBRETRO_VBAM_BUILD_CMDS
	$(SED) "s|-O2|-O3|g" $(@D)/src/libretro/Makefile
	CFLAGS="$(TARGET_CFLAGS) $(COMPILER_COMMONS_CFLAGS_SO)" \
		CXXFLAGS="$(TARGET_CXXFLAGS) $(COMPILER_COMMONS_CXXFLAGS_SO)" \
		LDFLAGS="$(TARGET_LDFLAGS) $(COMPILER_COMMONS_LDFLAGS_SO)" \
		$(MAKE) CXX="$(TARGET_CXX)" CC="$(TARGET_CC)" -C $(@D)/src/libretro -f Makefile platform=$(LIBRETRO_VBAM_PLATFORM) $(LIBRETRO_VBAM_NEON)
endef

define LIBRETRO_VBAM_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/src/libretro/vbam_libretro.so \
		$(TARGET_DIR)/usr/lib/libretro/vbam_libretro.so
endef

$(eval $(generic-package))
