################################################################################
#
# VBA-NEXT
#
################################################################################

LIBRETRO_VBA_NEXT_VERSION = f21aaeeee9c84f18c08ef59f22bc02d34c565ad9
LIBRETRO_VBA_NEXT_SITE = $(call github,libretro,vba-next,$(LIBRETRO_VBA_NEXT_VERSION))

ifeq ($(BR2_ARM_CPU_HAS_NEON),y)
LIBRETRO_VBA_NEXT_NEON += "HAVE_NEON=1"
else
LIBRETRO_VBA_NEXT_NEON += "HAVE_NEON=0"
endif

ifeq ($(BR2_arm),y)
LIBRETRO_VBA_NEXT_PLATFORM=classic_armv7_a7
else
LIBRETRO_VBA_NEXT_PLATFORM=$(RETROARCH_LIBRETRO_PLATFORM)
endif

define LIBRETRO_VBA_NEXT_BUILD_CMDS
	$(SED) "s|-O2|-O3|g" $(@D)/Makefile.libretro
	CFLAGS="$(TARGET_CFLAGS) $(COMPILER_COMMONS_CFLAGS_SO)" \
		CXXFLAGS="$(TARGET_CXXFLAGS) $(COMPILER_COMMONS_CXXFLAGS_SO)" \
		LDFLAGS="$(TARGET_LDFLAGS) $(COMPILER_COMMONS_LDFLAGS_SO)" \
		$(MAKE) CXX="$(TARGET_CXX)" CC="$(TARGET_CC)" -C $(@D)/ -f Makefile.libretro platform=$(LIBRETRO_VBA_NEXT_PLATFORM) $(LIBRETRO_VBA_NEXT_NEON)
endef

define LIBRETRO_VBA_NEXT_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/vba_next_libretro.so \
		$(TARGET_DIR)/usr/lib/libretro/vba_next_libretro.so
endef

$(eval $(generic-package))
